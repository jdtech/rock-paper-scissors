Yet *another* rock paper scissors game coded in HTML and JavaScript :D

Changelog

    v1.0.0 - initial version
    *v1.0.1 - added more debug and versioning info*
    v1.1.0 - now shows player's and computer's moves
