var version = "v1.1.0";
var debug = false;

document.getElementById("version").innerHTML = version;

function playerGuess() {
    switch(document.getElementById('guess').value) {
        case "rock":
            return 1;
        case "paper":
            return 2;
        case "scissors":
            return 3;
        default:
            return "ERROR!";
    }
}

function RPS() {
    var CPU = Math.floor(Math.random() * (3 - 1 + 1) + 1);
    var PLAYER = playerGuess();
    var VALUE = "" + CPU + PLAYER;
    if (debug === true) {
        console.log("JD.RPS " + version + "\n----------------------------------------------");
        console.log("Computer's guess: " + CPU);
        console.log("Player's guess: " + PLAYER);
        console.log("Both: " + VALUE);
    }
    else {
        console.log("JD.RPS " + version + "\n----------------------------------------------");
        console.log("Debug off. Set debug variable to true to enable debug mode.");
    }

    // 1 is rock, 2 is paper, 3 is scissors
    // First number is computer, second is player
    switch(VALUE) {
        case "11":
            document.getElementById("result2").innerHTML = "You played: ROCK. " + "The computer played: ROCK.";
            return "Tie!";
        case "12":
            document.getElementById("result2").innerHTML = "You played: PAPER. " + "The computer played: ROCK.";
            return "Win!";
        case "13":
            document.getElementById("result2").innerHTML = "You played: SCISSORS. " + "The computer played: ROCK.";
            return "Loss!";
        case "21":
            document.getElementById("result2").innerHTML = "You played: ROCK. " + "The computer played: PAPER.";
            return "Loss!";
        case "22":
            document.getElementById("result2").innerHTML = "You played: PAPER. " + "The computer played: PAPER.";
            return "Tie!";
        case "23":
            document.getElementById("result2").innerHTML = "You played: SCISSORS. " + "The computer played: PAPER.";
            return "Win!";
        case "31":
            document.getElementById("result2").innerHTML = "You played: ROCK. " + "The computer played: SCISSORS.";
            return "Win!";
        case "32":
            document.getElementById("result2").innerHTML = "You played: PAPER. " + "The computer played: SCISSORS.";
            return "Loss!";
        case "33":
            document.getElementById("result2").innerHTML = "You played: SCISSORS. " + "The computer played: SCISSORS.";
            return "Tie!";
        default:
            return "ERROR!";
    }
}

function startGame() {
    document.getElementById("result").innerHTML = RPS();
}
